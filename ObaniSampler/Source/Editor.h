/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "Label.h"
#include "Processor.h"
#include "SampleEditor.h"
#include "SampleTimeline.h"
#include "Slider.h"
#include "ToggleTextButton.h"
#include "ToggleImageButton.h"

//==============================================================================
/**
*/
class Editor : public juce::AudioProcessorEditor,
               public juce::Button::Listener {
 public:
  Editor(Processor&, VTS& vts,
               const juce::File& defaultFile);

 private:
  static constexpr int editorMinHeight = 200;
  static constexpr int controlMinWidth = 100;
  static constexpr int sampleWidth = 500;
  static constexpr int kbHeight = 100; 

  static constexpr float OCTAVE_RANGE = MAX_OCTAVE - MIN_OCTAVE;

  void buttonClicked(juce::Button*);
  void setOctaveComboBox();
  void resized() override;

  Processor& audioProcessor;
  VTS& vts;
  
  juce::TooltipWindow tooltips;

  juce::ComboBox scale;
  juce::TextButton octaveMinus, octavePlus;

  ToggleImageButton invert, loop;
  Slider gain;

  juce::MidiKeyboardComponent keyboard;
  SampleEditor sample;

  VTS::ComboBoxAttachment scaleAttach;
  VTS::SliderAttachment gainAttach;

  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Editor)
};
