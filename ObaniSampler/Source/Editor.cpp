/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "Editor.h"
#include "Processor.h"

//==============================================================================
Editor::Editor(Processor &p, VTS &vts, const juce::File &defaultFile)
    : AudioProcessorEditor(&p),
      audioProcessor(p),
      vts(vts),
      tooltips(this, 0),
      scale(SCALE_ID),
      octaveMinus("-"),
      octavePlus("+"),
      invert(INVERT_ID, vts.getParameter(INVERT_ID),
             {juce::ImageCache::getFromMemory(BinaryData::forward_png,
                                              BinaryData::forward_pngSize),
              juce::ImageCache::getFromMemory(BinaryData::backward_png,
                                              BinaryData::backward_pngSize)}),
      loop(LOOP_ID, vts.getParameter(LOOP_ID),
           {juce::ImageCache::getFromMemory(BinaryData::noloop_png,
                                            BinaryData::noloop_pngSize),
            juce::ImageCache::getFromMemory(BinaryData::loop_png,
                                            BinaryData::loop_pngSize)}),
      gain("Gain"),
      keyboard(p.midiState,
               juce::KeyboardComponentBase::Orientation::horizontalKeyboard),
      sample(p),
      scaleAttach(vts, SCALE_ID, scale),
      gainAttach(vts, GAIN_ID, gain) {
  setResizeLimits(sampleWidth + controlMinWidth, kbHeight + editorMinHeight,
                  4096, 4096);
  setResizable(true, false);

  scale.addItemList(SCALE_ARRAY, 1);
  scale.setSelectedId(4);
  sample.setFile(defaultFile);

  setOctaveComboBox();
  octaveMinus.addListener(this);
  octavePlus.addListener(this);

  addAndMakeVisible(sample);
  addAndMakeVisible(keyboard);
  addAndMakeVisible(scale);
  addAndMakeVisible(octaveMinus);
  addAndMakeVisible(octavePlus);
  addAndMakeVisible(invert);
  addAndMakeVisible(loop);
  addAndMakeVisible(gain);
}

void Editor::buttonClicked(juce::Button* btn) {
  juce::RangedAudioParameter* param = vts.getParameter(OCTAVE_ID);
  float inc = 1.f/OCTAVE_RANGE;
  param->beginChangeGesture();
  param->setValueNotifyingHost(param->getValue() +
                               (btn->getButtonText() == "-" ? -inc : inc));
  param->endChangeGesture();
  setOctaveComboBox();
}

void Editor::setOctaveComboBox() {
  int octave = OCTAVE_RANGE * vts.getParameter(OCTAVE_ID)->getValue();
  int selected = scale.getSelectedId();

  for (int i = 0; i < SCALE_ARRAY.size(); i++) {
    scale.changeItemText(i + 1, SCALE_ARRAY[i] + juce::String(octave));
  }

  scale.setText(SCALE_ARRAY[selected - 1] + juce::String(octave));
}

void Editor::resized() {
  auto bounds = getLocalBounds();
  keyboard.setBounds(bounds.removeFromBottom(kbHeight));

  sample.setBounds(bounds);

  auto upBounds = bounds.removeFromTop(bounds.getHeight()/3);
  invert.setBounds(upBounds.removeFromLeft(upBounds.getHeight()).reduced(upBounds.getHeight()/6));
  loop.setBounds(upBounds.removeFromRight(upBounds.getHeight()).reduced(upBounds.getHeight()/6));


  auto downBounds = bounds.removeFromBottom(bounds.getHeight()/4);
  auto scaleBounds = downBounds.removeFromRight(downBounds.getWidth()/4);
  octaveMinus.setBounds(scaleBounds.removeFromLeft(scaleBounds.getWidth()/5));
  octavePlus.setBounds(scaleBounds.removeFromRight(octaveMinus.getWidth()));
  scale.setBounds(scaleBounds);

  gain.setBounds(downBounds.removeFromRight(scaleBounds.getWidth()));
}
