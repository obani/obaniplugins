#pragma once

#include <JuceHeader.h>

using SliderStyle = juce::Slider::SliderStyle;
using TextEntryBoxPosition = juce::Slider::TextEntryBoxPosition;

class Slider : public juce::Slider {


 public:
  Slider(const juce::String& name)
      : juce::Slider(SliderStyle::LinearHorizontal,
                     TextEntryBoxPosition::NoTextBox) {
    setTooltip(name);
    setColour(juce::Slider::textBoxOutlineColourId, juce::Colour());
  }
};