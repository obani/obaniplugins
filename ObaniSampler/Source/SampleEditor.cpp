#include "SampleEditor.h"

SampleEditor::SampleEditor(Processor& p)
    : processor(p),
      fileChooser(nullptr),
      fileLoad("..."),
      fileName("file_name", ""),
      imageComponent("sample_image"),
      attack(processor.params.getParameter(ATTACK_ID)->getValue()),
      release(processor.params.getParameter(RELEASE_ID)->getValue()),
      startSample(processor.params.getParameter(START_SAMPLE_ID)->getValue()),
      endSample(processor.params.getParameter(END_SAMPLE_ID)->getValue()),
      grabbed_str(&ATTACK_ID) {
  imageComponent.setInterceptsMouseClicks(false, false);
  imageComponent.setImagePlacement(juce::RectanglePlacement::stretchToFit);
  fileName.setFont(juce::Font(30));
  addAndMakeVisible(imageComponent);
  addAndMakeVisible(fileLoad);
  addAndMakeVisible(fileName);

  fileLoad.addListener(this);

  processor.params.addParameterListener(ATTACK_ID, this);
  processor.params.addParameterListener(RELEASE_ID, this);
  processor.params.addParameterListener(START_SAMPLE_ID, this);
  processor.params.addParameterListener(END_SAMPLE_ID, this);
}

void SampleEditor::updateState(const juce::StringRef& id, float value) {
  processor.setBoundsParam(id, value);
}


void SampleEditor::mouseDown(const juce::MouseEvent& e) {
  float x = float(e.x) / getWidth();
  grabbed_str = e.mods.isRightButtonDown()
                    ? std::abs(x - startSample) < std::abs(x - endSample) ||
                              x < startSample
                          ? &START_SAMPLE_ID
                          : &END_SAMPLE_ID
                : std::abs(x - attack) < std::abs(x - release) || x < attack
                    ? &ATTACK_ID
                    : &RELEASE_ID;
  updateState(*grabbed_str, x);
}

void SampleEditor::mouseDrag(const juce::MouseEvent& e) {
  updateState(*grabbed_str, float(e.x) / getWidth());
}

void SampleEditor::parameterChanged(const juce::String& id, float v) {
  if (id == ATTACK_ID) {
    attack = v;
  } else if (id == RELEASE_ID) {
    release = v;
  } else if (id == START_SAMPLE_ID) {
    startSample = v;
  } else if (id == END_SAMPLE_ID) {
    endSample = v;
  } else {
    return;
  }

  repaint();
}

void SampleEditor::paintImage() {
  image = juce::Image(juce::Image::PixelFormat::SingleChannel, getWidth() * 4,
                      getHeight() * 2, true);

  std::unique_ptr<juce::LowLevelGraphicsContext> g = image.createLowLevelContext();

  const int numChannels = processor.fileBuffer.getNumChannels();
  const size_t numSamples = processor.fileBuffer.getNumSamples();
  const size_t width = image.getWidth();
  const size_t height = image.getHeight();
  const int chanHeight = height/numChannels;
  const int amplitude = chanHeight/2;

  for (int c = 0; c < numChannels; c++) {
    const int center = chanHeight * c + amplitude;
    const float* data = processor.fileBuffer.getReadPointer(c);
    for (size_t x = 0; x < width; x++) {
      float y = std::log10(1.f + std::abs(data[(x * numSamples)/width]) * 9.f) * amplitude;
      g->drawLine(juce::Line<float>(x, center - y, x, center + y));
    }
  }

  imageComponent.setImage(image);
}

void SampleEditor::paint(juce::Graphics& g) {
  static const juce::Colour cutFill =
      juce::Colour(uint8_t(255), uint8_t(200), uint8_t(0), uint8_t(100));
  static const juce::Colour envelopeColour = juce::Colour(uint8_t(255), uint8_t(255), uint8_t(255));
  static const int lineThickness = 2;

  const int w = getWidth();
  int h = getHeight();

  g.setGradientFill(juce::ColourGradient{
      juce::Colours::white.withAlpha(0.3f), 0, 0,
      juce::Colours::black.withAlpha(0.5f), 0, float(h), false});
  g.fillRect(0, 0, w, h);

  g.setColour(cutFill);
  g.fillRect(0, 0, int(w * startSample), h);
  g.fillRect(int(w * endSample), 0, w, h);

  const int bot = h - lineThickness / 2;
  const int top = lineThickness / 2;
  juce::Path p;
  p.startNewSubPath(0, bot);
  p.lineTo(w * startSample, bot);
  p.lineTo(w * attack, top);
  p.lineTo(w * release, top);
  p.lineTo(w * endSample, bot);
  p.lineTo(w, bot);

  g.setColour(envelopeColour.withAlpha(0.2f));
  g.fillPath(p);

  g.setColour(envelopeColour);
  g.strokePath(p, juce::PathStrokeType(lineThickness));
}

void SampleEditor::resized() {
  auto bounds = getBounds();
  imageComponent.setBounds(bounds);

  auto downBounds = bounds.removeFromBottom(bounds.getHeight()/6);
  fileLoad.setBounds(downBounds.removeFromLeft(downBounds.getHeight()).reduced(2));
  fileName.setBounds(downBounds);
}

void SampleEditor::setFile(const juce::File& file) {
  FileError err = processor.readFile(file);

  if (err == FileError::NotExist) {
    printf("Couldn't read file\n");
  } else if (err == FileError::UnknownFormat) {
    printf("Unknown audio file format\n");
  } else {
    paintImage();
    fileName.setText(file.getFileName(), juce::NotificationType::sendNotification);
    std::cout << "File " << file.getFullPathName() << " loaded\n";
  }
}

void SampleEditor::fileChoose() {
  fileChooser = std::make_unique<juce::FileChooser>(
      "Select an audio file...", processor.audioFile,
      processor.formatManager.getWildcardForAllFormats());
  auto flags = juce::FileBrowserComponent::openMode;

  fileChooser->launchAsync(flags, [this](const juce::FileChooser& chooser) {
    setFile(chooser.getResult());
  });
}

void SampleEditor::mouseDoubleClick(const juce::MouseEvent& e) { fileChoose(); }
void SampleEditor::buttonClicked(juce::Button* btn) { fileChoose(); }

bool SampleEditor::isInterestedInFileDrag(const juce::StringArray& files) {
  return true;
}

void SampleEditor::filesDropped(const juce::StringArray& files, int x, int y) {
  setFile(juce::File(files[0]));
}