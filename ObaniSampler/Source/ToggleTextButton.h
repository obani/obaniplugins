#pragma once

#include <JuceHeader.h>

class ToggleTextButton : public juce::TextButton,
                         public juce::Button::Listener {
 public:
  ToggleTextButton(const juce::String& id, juce::RangedAudioParameter* p,
                   const juce::StringArray& opts)
      : TextButton(opts[int(p->getValue() * (opts.size() - 1))], id),
        selected(p->getValue() * (opts.size() - 1)),
        options(opts),
        param(p) {
    addListener(this);
  }

  void buttonClicked(juce::Button* btn) {
    selected = (selected + 1) % options.size();
    setButtonText(options[selected]);

    if (param != nullptr) {
      param->beginChangeGesture();
      param->setValueNotifyingHost(float(selected) / (options.size() - 1));
      param->endChangeGesture();
    }
  }

 private:
  int selected;
  juce::StringArray options;
  juce::RangedAudioParameter* param;
};