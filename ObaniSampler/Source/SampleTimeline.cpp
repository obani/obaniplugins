#include "SampleTimeline.h"

SampleTimeline::SampleTimeline(const Processor& p)
    : processor(p),
      vblank(this, [this]() { this->repaint(); }) {
  removeMouseListener(this);
  setInterceptsMouseClicks(false, false);
}

void SampleTimeline::paint(juce::Graphics& g) {
  int posX = (getWidth() * (size_t)processor.getCurrentSample()) /
             processor.getNumSamples();
  g.setColour(juce::Colours::red);
  g.drawLine(posX, 0, posX, getHeight(), 1);
}