#pragma once

#include <JuceHeader.h>
#include <vector>

class ToggleImageButton : public juce::ImageButton,
                          public juce::Button::Listener {
  void setImage() {
    static const juce::Colour c(255.f, 255.f, 255.f, 0.f);

    const juce::Image& img = images[selected];
    setImages(false, true, true, img, 0.6f, c, img, 0.75f, c, img, 1.f, c, 0.f);
  }

 public:
  ToggleImageButton(const juce::String& id, juce::RangedAudioParameter* p,
                    const std::vector<juce::Image>& imgs)
      : ImageButton(id), selected(p->getValue() * (imgs.size() - 1)), param(p) {
    for (const juce::Image& img : imgs) {
      images.push_back(img);
    }

    setImage();
    addListener(this);
  }

  void buttonClicked(juce::Button* btn) {
    selected = (selected + 1) % images.size();
    setImage();

    if (param != nullptr) {
      param->beginChangeGesture();
      param->setValueNotifyingHost(float(selected) / (images.size() - 1));
      param->endChangeGesture();
    }
  }

 private:
  int selected;
  juce::RangedAudioParameter* param;
  std::vector<juce::Image> images;
};