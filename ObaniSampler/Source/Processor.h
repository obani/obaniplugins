/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

enum FileError { None, NotExist, UnknownFormat };

using VTS = juce::AudioProcessorValueTreeState;
using f = float;

static const juce::String FILE_ID = "file_name";
static const juce::String SCALE_ID = "scale";
static const juce::String OCTAVE_ID = "octave";
static const juce::String INVERT_ID = "invert";
static const juce::String LOOP_ID = "loop";
static const juce::String GAIN_ID = "gain";
static const juce::String START_SAMPLE_ID = "start_sample";
static const juce::String END_SAMPLE_ID = "end_sample";
static const juce::String ATTACK_ID = "attack";
static const juce::String RELEASE_ID = "release";

static const juce::StringArray SCALE_ARRAY = {
    "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"};
static const std::array<int, 12> SCALE_MIDI = {49, 50, 51, 52, 53, 54,
                                               55, 56, 57, 58, 59, 60};


constexpr int MIN_OCTAVE = -3;
constexpr int MAX_OCTAVE = 3;

//==============================================================================
/**
*/
class Processor : public juce::AudioProcessor,
                  public juce::MidiKeyboardState::Listener
#if JucePlugin_Enable_ARA
    ,
                  public juce::AudioProcessorARAExtension
#endif
{
 public:
  const juce::String programName{"ObaniSampler"};

  //==============================================================================
  Processor();

  void setBoundsParam(const juce::StringRef& id, float value);

  //==============================================================================
  void prepareToPlay(double sampleRate, int samplesPerBlock) override;

#ifndef JucePlugin_PreferredChannelConfigurations
  bool isBusesLayoutSupported(const BusesLayout& layouts) const override;
#endif

  void handleNoteOn(juce::MidiKeyboardState* source, int midiChannel,
                    int midiNoteNumber, float velocity);
  void handleNoteOff(juce::MidiKeyboardState* source, int midiChannel,
                     int midiNoteNumber, float velocity);

  int processFile(juce::AudioBuffer<float>& buffer, int bufferStart,
                  int bufferSamples, int start, int end, f atkFact,
                  int atkStart, f rlsFact, int rlsStart, f speed, f gain);
  void processBlock(juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

  //==============================================================================
  juce::AudioProcessorEditor* createEditor() override;
  bool hasEditor() const override;

  //==============================================================================
  const juce::String getName() const override;

  bool acceptsMidi() const override;
  bool producesMidi() const override;
  bool isMidiEffect() const override;
  double getTailLengthSeconds() const override;

  //==============================================================================
  int getNumPrograms() override;
  int getCurrentProgram() override;
  const juce::String getProgramName(int index) override;

  //==============================================================================
  void getStateInformation(juce::MemoryBlock& destData) override;
  void setStateInformation(const void* data, int sizeInBytes) override;

  FileError readFile(const juce::File& f);

  // unused
  void releaseResources() override{};
  void setCurrentProgram(int) override{};
  void changeProgramName(int, const juce::String&) override{};

  int getCurrentSample() const;
  int getNumSamples() const;

  juce::MidiKeyboardState midiState;
  juce::UndoManager undomanager;

  juce::AudioBuffer<float> fileBuffer;
  VTS params;

  juce::AudioFormatManager formatManager;
  juce::File audioFile;

 private:
  int limit(bool inv);
  void setValues();

  int pressed;
  bool playing;

  std::atomic<float>* scaleParam;
  std::atomic<float>* octaveParam;
  std::atomic<float>* invertParam;
  std::atomic<float>* loopParam;
  std::atomic<float>* gainParam;

  std::atomic<float>* attackParam;
  std::atomic<float>* releaseParam;
  std::atomic<float>* startSampleParam;
  std::atomic<float>* endSampleParam;

  float previousAttack, previousRelease;

  int currentSample;
  f step;
  //==============================================================================
  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Processor)
};
