#pragma once

#include <JuceHeader.h>
#include "Processor.h"

class SampleTimeline : public juce::Component {
 public:
  SampleTimeline(const Processor&);

  void onVBlank();
  void paint(juce::Graphics& g) override;
 private:
  const Processor& processor;
  juce::VBlankAttachment vblank;
};