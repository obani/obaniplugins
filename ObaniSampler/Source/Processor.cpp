/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "Processor.h"
#include "Editor.h"


using Range = juce::NormalisableRange<float>;

//==============================================================================
Processor::Processor()
#ifndef JucePlugin_PreferredChannelConfigurations
    : AudioProcessor(BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                         .withOutput("Output", juce::AudioChannelSet::stereo())
                     #endif
                         ),
      params(*this, &undomanager, juce::Identifier(programName),
             {
                 std::make_unique<juce::AudioParameterChoice>(
                     SCALE_ID,  // parameterID
                     "Scale",   // parameter name
                     SCALE_ARRAY, 0),
                 std::make_unique<juce::AudioParameterInt>(
                     OCTAVE_ID,                   // parameterID
                     "Octave",                    // parameter name
                     MIN_OCTAVE, MAX_OCTAVE, 0),  // range
                 std::make_unique<juce::AudioParameterBool>(
                     INVERT_ID,  // parameterID
                     "Invert",   // parameter name
                     false),     // default value
                 std::make_unique<juce::AudioParameterBool>(
                     LOOP_ID,  // parameterID
                     "Loop",   // parameter name
                     false),   // default value
                 std::make_unique<juce::AudioParameterFloat>(
                     GAIN_ID,                      // parameterID
                     "Gain",                       // parameter name
                     Range(0.f, 1.f, 0.f, 0.35f),  // range
                     1.f),                         // default value
                 std::make_unique<juce::AudioParameterFloat>(
                     START_SAMPLE_ID,       // parameterID
                     "Start Sample",        // parameter name
                     Range(0.f, 1.f, 0.f),  // range
                     0.f),                  // default value
                 std::make_unique<juce::AudioParameterFloat>(
                     END_SAMPLE_ID,         // parameterID
                     "End Sample",          // parameter name
                     Range(0.f, 1.f, 0.f),  // range
                     1.f),                  // default value
                 std::make_unique<juce::AudioParameterFloat>(
                     ATTACK_ID,             // parameterID
                     "Attack",              // parameter name
                     Range(0.f, 1.f, 0.f),  // range
                     0.f),                  // default value
                 std::make_unique<juce::AudioParameterFloat>(
                     RELEASE_ID,            // parameterID
                     "Release",             // parameter name
                     Range(0.f, 1.f, 0.f),  // range
                     1.f),                  // default value
             })
#endif
{
  formatManager.registerBasicFormats();

  scaleParam = params.getRawParameterValue(SCALE_ID);
  octaveParam = params.getRawParameterValue(OCTAVE_ID);
  invertParam = params.getRawParameterValue(INVERT_ID);
  loopParam = params.getRawParameterValue(LOOP_ID);
  gainParam = params.getRawParameterValue(GAIN_ID);

  attackParam = params.getRawParameterValue(ATTACK_ID);
  releaseParam = params.getRawParameterValue(RELEASE_ID);
  startSampleParam = params.getRawParameterValue(START_SAMPLE_ID);
  endSampleParam = params.getRawParameterValue(END_SAMPLE_ID);

  midiState.addListener(this);
}

void Processor::setBoundsParam(const juce::StringRef& id, float v) {
  juce::RangedAudioParameter* param = params.getParameter(id);

  if (id == ATTACK_ID) {
    previousAttack = std::clamp(v, startSampleParam->load(), releaseParam->load());
    param->setValueNotifyingHost(previousAttack);
  } else if (id == RELEASE_ID) {
    previousRelease = std::clamp(v, attackParam->load(), endSampleParam->load());
    param->setValueNotifyingHost(previousRelease);
  } else if (id == START_SAMPLE_ID) {
    juce::RangedAudioParameter* attackP = params.getParameter(ATTACK_ID);
    juce::RangedAudioParameter* releaseP = params.getParameter(RELEASE_ID);

    f startSample = std::clamp(v, 0.f, endSampleParam->load());
    param->setValueNotifyingHost(startSample);

    f attack = attackParam->load();
    if (startSample > previousAttack) { 
      attack = startSample;
      params.getParameter(ATTACK_ID)->setValueNotifyingHost(attack);
    }

    if (attack > previousRelease) {
      params.getParameter(RELEASE_ID)->setValueNotifyingHost(attack);
    }

  } else if (id == END_SAMPLE_ID) {
    f endSample = std::clamp(v, startSampleParam->load(), 1.f);
    param->setValueNotifyingHost(endSample);

    f release = releaseParam->load();
    if (endSample < previousRelease) {
      release = endSample;
      params.getParameter(RELEASE_ID)->setValueNotifyingHost(release);
    }

    if (release < previousAttack) {
      params.getParameter(ATTACK_ID)->setValueNotifyingHost(release);
    }
  }
}

//==============================================================================
const juce::String Processor::getName() const { return programName; }

bool Processor::acceptsMidi() const {
#if JucePlugin_WantsMidiInput
  return true;
#else
  return false;
#endif
}

bool Processor::producesMidi() const {
#if JucePlugin_ProducesMidiOutput
  return true;
#else
  return false;
#endif
}

bool Processor::isMidiEffect() const {
#if JucePlugin_IsMidiEffect
  return true;
#else
  return false;
#endif
}

double Processor::getTailLengthSeconds() const { return 0.0; }
int Processor::getNumPrograms() { return 1; }
int Processor::getCurrentProgram() { return 0; }

const juce::String Processor::getProgramName(int index) { return programName; }

//==============================================================================
void Processor::prepareToPlay(double sampleRate, int samplesPerBlock) {
  params.getParameter(ATTACK_ID)->setValueNotifyingHost(attackParam->load());
  params.getParameter(RELEASE_ID)->setValueNotifyingHost(releaseParam->load());
  params.getParameter(START_SAMPLE_ID)->setValueNotifyingHost(startSampleParam->load());
  params.getParameter(END_SAMPLE_ID)->setValueNotifyingHost(endSampleParam->load());

  previousAttack = attackParam->load();
  previousRelease = releaseParam->load();

  playing = false;
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Processor::isBusesLayoutSupported(const BusesLayout& layouts) const {
#if JucePlugin_IsMidiEffect
  juce::ignoreUnused(layouts);
  return true;
#else
  // This is the place where you check if the layout is supported.
  // In this template code we only support mono or stereo.
  // Some plugin hosts, such as certain GarageBand versions, will only
  // load plugins that support stereo bus layouts.
  if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono() &&
      layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
    return false;

    // This checks if the input layout matches the output layout
#if !JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
#endif

    return true;
#endif
}
#endif

constexpr f crossfade(f x, f y, f fade) { return x + (y - x) * fade; }

int Processor::limit(bool inv) { return inv ? -1 : getNumSamples(); }

void Processor::handleNoteOn(juce::MidiKeyboardState* source, int midiChannel,
                             int midiNoteNumber, float velocity) {
  if (getNumSamples() == 0) return;

  step = 0;
  currentSample = bool(invertParam->load()) ? getNumSamples() - 1 : 0;
  pressed = midiNoteNumber;
  playing = true;
}

void Processor::handleNoteOff(juce::MidiKeyboardState* source, int midiChannel,
                              int midiNoteNumber, float velocity) {
  playing = playing && midiNoteNumber != pressed;
}

int Processor::processFile(juce::AudioBuffer<float>& buffer, int bufferStart,
                           int bufferSamples, int start, int end,
                           f attackFactor, int attackEnd, f releaseFactor,
                           int releaseStart, f speed, f gain) {
  const int numOutputChannels = getTotalNumOutputChannels();
  const int numInputChannels = fileBuffer.getNumChannels();

  bool inv = bool(invertParam->load());

  int bufferSize = std::min(
      bufferSamples - bufferStart,
      std::max(1, int(std::abs(step) +
                      f((inv ? start : end) - currentSample) / speed)));

  double newStep = step;
  int startSample = inv ? -(bufferSize * speed - step) : 0;


  for (int channel = 0; channel < numOutputChannels; ++channel) {
    auto* in = fileBuffer.getWritePointer(channel % numInputChannels,
                                          currentSample - startSample);
    auto* out = buffer.getWritePointer(channel, bufferStart);

    newStep = step;
    int inIdx, newCurrent;
    for (int i = 0; i < bufferSize; i++, newStep += speed) {
      newCurrent = currentSample + newStep;
      inIdx = newStep + startSample;

      out[i] = in[inIdx] * gain *
               (newCurrent < attackEnd ? (newCurrent - start) * attackFactor
                : newCurrent > releaseStart
                    ? (1.f - (newCurrent - releaseStart) * releaseFactor)
                    : 1.0f);
    }
  }

  currentSample += newStep;
  if (currentSample >= end || currentSample <= start) currentSample = limit(inv);
  step = newStep - (int)newStep;
  return bufferStart + bufferSize;
}

void Processor::processBlock(juce::AudioBuffer<float>& buffer,
                             juce::MidiBuffer& midiMessages) {
  if(!playing) return;

  juce::ScopedNoDenormals noDenormals;
  const int numOutputChannels = getTotalNumOutputChannels();

  int bufferSamples = buffer.getNumSamples();
  for (auto i = 0; i < numOutputChannels; ++i) {
    buffer.clear(i, 0, bufferSamples);
  }

  midiState.processNextMidiBuffer(midiMessages, midiMessages.getFirstEventTime(), midiMessages.getLastEventTime(), false);

  int numSamples = fileBuffer.getNumSamples();

  bool inv = bool(invertParam->load());
  if ((inv && currentSample < 0) || (!inv && currentSample >= numSamples))
    currentSample = limit(inv);

  f startSample =
      std::clamp(startSampleParam->load(), 0.f, endSampleParam->load());
  f endSample = std::clamp(endSampleParam->load(), startSample, 1.f);
  f attack = std::clamp(attackParam->load(), startSample, releaseParam->load());
  f release = std::clamp(releaseParam->load(), attack, endSample);

  const int note =
      pressed - SCALE_MIDI[int(scaleParam->load())] + 12 * octaveParam->load();

  f speed = std::pow(2.f, note / 12.f) * (1.f - (invertParam->load() * 2.f));

  int start = int(startSample * numSamples);
  int end = int(endSample * numSamples);
  int attackEnd = int(attack * numSamples);
  int releaseStart = int(release * numSamples);

  currentSample = std::clamp(currentSample, start, end); // clamp to start

  f attackFactor = attackEnd == start ? 1.f : 1.f/(attackEnd - start); //one division is better than multiple ones
  f releaseFactor = releaseStart == end ? 1.f : 1.f/(end - releaseStart);
  f gain = gainParam->load();

  int bufferStart =
      processFile(buffer, 0, bufferSamples, start, end, attackFactor, attackEnd,
                  releaseFactor, releaseStart, speed, gain);

  playing = bool(loopParam->load()) || bufferStart == bufferSamples;

  while (playing && bufferStart != bufferSamples) {
    currentSample = inv ? end : start;
    bufferStart =
        processFile(buffer, bufferStart, bufferSamples, start, end,
                    attackFactor, attackEnd, releaseFactor, releaseStart, speed, gain);
  }
}

//==============================================================================
bool Processor::hasEditor() const { return true; }

juce::AudioProcessorEditor* Processor::createEditor() {
  return new Editor(*this, params, audioFile);
}

//==============================================================================
void Processor::getStateInformation(juce::MemoryBlock& destData) {
  juce::ValueTree state = params.copyState();
  std::unique_ptr<juce::XmlElement> xml (state.createXml());
  xml->setAttribute(FILE_ID, audioFile.getFullPathName());
  copyXmlToBinary(*xml, destData);
}

void Processor::setStateInformation(const void* data, int sizeInBytes) {
  std::unique_ptr<juce::XmlElement> xmlState(
        getXmlFromBinary(data, sizeInBytes));

  if (xmlState.get() != nullptr) {
    if (xmlState->hasTagName(params.state.getType())) {
      params.replaceState(juce::ValueTree::fromXml(*xmlState));
    }
    audioFile = juce::File(xmlState->getStringAttribute(FILE_ID));
  }
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter() {
  return new Processor();
}

FileError Processor::readFile(const juce::File& file) {
  if (!file.existsAsFile()) return FileError::NotExist;

  std::unique_ptr<juce::AudioFormatReader> reader(formatManager.createReaderFor(file));
  if (reader == nullptr) return FileError::UnknownFormat;

  audioFile = file;

  fileBuffer.setSize(reader->numChannels, reader->lengthInSamples, false, false, true);
  reader->read(&fileBuffer, 0, reader->lengthInSamples, 0, true, true);
  playing = false;

  return FileError::None;
}

int Processor::getCurrentSample() const { return currentSample; }
int Processor::getNumSamples() const { return fileBuffer.getNumSamples(); }