#pragma once

#include <JuceHeader.h>
#include "Processor.h"
#include "Label.h"

class SampleEditor : public juce::Component,
                     public juce::FileDragAndDropTarget,
                     public VTS::Listener,
                     public juce::Button::Listener {
 public:
  SampleEditor(Processor&);
  void paintImage();
  void setFile(const juce::File& file);
 private:
  void parameterChanged(const juce::String& parameterID, float newValue);

  void updateState(const juce::StringRef& id, float value);

  void mouseDown(const juce::MouseEvent& e);
  void mouseDrag(const juce::MouseEvent& e);

  void paint(juce::Graphics& g) override;
  void resized() override;

  void fileChoose();
  void mouseDoubleClick(const juce::MouseEvent& e);
  void buttonClicked(juce::Button* btn);
  
  bool isInterestedInFileDrag(const juce::StringArray& files);
  void filesDropped(const juce::StringArray& files, int x, int y);

  Processor& processor;
  std::unique_ptr<juce::FileChooser> fileChooser;
  juce::TextButton fileLoad;
  Label fileName;
  juce::ImageComponent imageComponent;
  juce::Image image;

  float attack, release, startSample, endSample;

  const juce::String* grabbed_str;
};