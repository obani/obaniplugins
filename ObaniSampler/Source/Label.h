#pragma once

#include <JuceHeader.h>

class Label : public juce::Label {
 public:
  Label(const juce::String& id, const juce::String& text)
      : juce::Label(id, text) {
    setInterceptsMouseClicks(false, false);
  }
};