#!/bin/sh

if [ $# == 3 ]; then
	cd "$1"
	make "CONFIG=$3" && ./build/$2;
	cd ../../
fi