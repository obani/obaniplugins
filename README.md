# ObaniPlugins


LV2/VST/Standalone plugins I work on with JUCE probably
All the code that isn't JUCE generated in this repo is licensed under GPLv3

Plugins:
- ObaniSampler (WIP): a replacement for samplv1


# Build

Download juce https://juce.com/get-juce/ and put the `JUCE` folder in your home directory

Each Plugin has a `Builds/LinuxMakefile` directory in which you can run the Makefile using the `make` command.